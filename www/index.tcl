# /packages/xml-rpc/www/index.tcl
ad_page_contract {
    Accept XML-RPC POST requests and processes them. GET requests are shown
    links to the admin pages or docs.

    @author Vinod Kurup [vinod@kurup.com]
    @creation-date Mon Sep 29 23:35:14 2003
    @cvs-id $Id: index.tcl,v 1.1.1.1 2006/06/29 21:11:32 cvs Exp $
} {
}

if {[string equal [ns_conn method] POST]} {
    set content [ns_conn content]
    ns_return 200 text/xml [xmlrpc::invoke $content]
    return
}

# GET requests fall through to index.adp
